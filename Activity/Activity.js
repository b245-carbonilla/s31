// Part 1

// 1. What directive is used by Node.js in loading the modules it needs?

Ans: require

// 2. What Node.js module contains a method for server creation?

Ans: http


// 3. What is the method of the http object responsible for creating a server using Node.js?

Ans: createServer()

// 4. What method of the response object allows us to set status codes and content types?

Ans: response.writeHead();

// 5. Where will console.log() output its contents when run in Node.js?

Ans: Command Line

// 6. What property of the request object contains the address's endpoint?

Ans: request.url
